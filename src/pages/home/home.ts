import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { PiServiceProvider } from '../../providers/pi-service/pi-service';
import { AssetDatabasesPage } from '../../pages/asset-databases/asset-databases';
import { LoginPage } from '../../pages/login/login';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  servers:any;
  constructor(public navCtrl: NavController, public auth: AuthProvider, private piService: PiServiceProvider, private alertCtrl : AlertController) {

  }

  ionViewDidLoad() {
    this.piService.getAssetServers().subscribe(
      response => {
        this.servers = response['data'];
      }
    );
  }

  viewDatabasesPage(server) {
    window.localStorage.setItem('asset_server_webid', server.WebId);
    this.navCtrl.push(AssetDatabasesPage, server);
  }

  logout() {
    let alert = this.alertCtrl.create({
      title : 'Logout Confirmation',
      message : 'Are you sure you want to logout?',
      buttons : [
        {
          text : 'No',
          role : 'cancel'
        },
        {
          text : 'Yes',
          handler : () => {
            window.localStorage.setItem('user', '');
            this.navCtrl.setRoot(LoginPage);
          }
        }
      ]
    });
    alert.present();
  }


}
