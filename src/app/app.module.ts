import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { AssetDatabasesPage } from '../pages/asset-databases/asset-databases';
import { AssetElementsPage } from '../pages/asset-elements/asset-elements';
import { AttributesPage } from '../pages/attributes/attributes';
import { ChildElementsPage } from '../pages/child-elements/child-elements';
import { AttributeDetailsPage } from '../pages/attribute-details/attribute-details';

import { AuthProvider } from '../providers/auth/auth';
import { HttpClientModule } from '@angular/common/http'; 
import { HttpModule } from '@angular/http';
import { GlobalProvider } from '../providers/global/global';
import { PiServiceProvider } from '../providers/pi-service/pi-service';
import { AddElementPage } from '../pages/add-element/add-element';
import { AddAttributePage } from '../pages/add-attribute/add-attribute';
import { AttributePropertiesPage } from '../pages/attribute-properties/attribute-properties';
import { AttributeTypesPage } from '../pages/attribute-types/attribute-types';
import { AttributeUomPage } from '../pages/attribute-uom/attribute-uom';
import { AttributeChildUomPage } from '../pages/attribute-child-uom/attribute-child-uom';
import { AttributeDataReferencePage } from '../pages/attribute-data-reference/attribute-data-reference';
import { AttributeCategoriesPage } from '../pages/attribute-categories/attribute-categories';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    AssetDatabasesPage,
    AssetElementsPage,
    AttributesPage,
    ChildElementsPage,
    AttributeDetailsPage,
    AddElementPage,
    AddAttributePage,
    AttributePropertiesPage,
    AttributeTypesPage,
    AttributeUomPage,
    AttributeChildUomPage,
    AttributeDataReferencePage,
    AttributeCategoriesPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    AssetDatabasesPage,
    AssetElementsPage,
    AttributesPage,
    ChildElementsPage,
    AttributeDetailsPage,
    AddElementPage,
    AddAttributePage,
    AttributePropertiesPage,
    AttributeTypesPage,
    AttributeUomPage,
    AttributeChildUomPage,
    AttributeDataReferencePage,
    AttributeCategoriesPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    GlobalProvider,
    PiServiceProvider
  ]
})
export class AppModule {}
